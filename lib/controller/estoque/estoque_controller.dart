import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:escala_cafe_app/model/estoque.dart';
import 'package:escala_cafe_app/model/produto.dart';

class EstoqueController {

  Future<List<Estoque>> getEstoque() async {

    List<Estoque> estoques = List();

    QuerySnapshot estoquesJson = await Firestore.instance.collection("estoques").getDocuments();
    for (DocumentSnapshot document in estoquesJson.documents){
      estoques.add(Estoque.fromDocument(document));
    }

    for (Estoque estoque in estoques){
      DocumentSnapshot document = await Firestore.instance.collection("produtos").document(estoque.produtoReference.documentID).get();
      Produto produto = Produto.fromDocument(document);
      estoque.produto = produto;
    }

    return estoques;
  }

}