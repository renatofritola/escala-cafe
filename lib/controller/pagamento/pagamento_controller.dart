import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:escala_cafe_app/model/pagamento.dart';
import 'package:escala_cafe_app/model/participante_pagamento.dart';
import 'package:escala_cafe_app/model/pessoa.dart';

class PagamentoController {

  Future<Pagamento> getPagamentoParticipantes() async {

    Query pagamentosQuery = await Firestore.instance.collection("pagamentos");

    QuerySnapshot pagamentos   = await pagamentosQuery.getDocuments();
    Pagamento pagamentoVigente = Pagamento.fromDocument(pagamentos.documents.first);
    pagamentoVigente.participantes = List();

    var participantes = await Firestore.instance.collection("pagamentos")
                                                .document(pagamentoVigente.id)
                                                .collection("participantes")
                                                .getDocuments();

    for(DocumentSnapshot doc in participantes.documents){
      var p = ParticipantePagamento.fromDocument(doc);
      var pessoa = await Firestore.instance.collection("pessoas").document(p.pessoaReference.documentID).get();
      p.pessoa = Pessoa.fromDocument(pessoa);
      pagamentoVigente.participantes.add(p);
    }

    return pagamentoVigente;
  }

}