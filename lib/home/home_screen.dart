import 'dart:async';

import 'package:escala_cafe_app/controller/escala/escala_controller.dart';
import 'package:escala_cafe_app/controller/estoque/estoque_controller.dart';
import 'package:escala_cafe_app/controller/geral/geral_controller.dart';
import 'package:escala_cafe_app/controller/pagamento/pagamento_controller.dart';
import 'package:escala_cafe_app/model/cafezeiro.dart';
import 'package:escala_cafe_app/model/enumerators/dia_semana.dart';
import 'package:escala_cafe_app/model/enumerators/dia_semana_helper.dart';
import 'package:escala_cafe_app/model/escala.dart';
import 'package:escala_cafe_app/model/estoque.dart';
import 'package:escala_cafe_app/model/geral.dart';
import 'package:escala_cafe_app/model/pagamento.dart';
import 'package:escala_cafe_app/model/participante_pagamento.dart';
import 'package:escala_cafe_app/util/AppColors.dart';
import 'package:escala_cafe_app/util/date_helper.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:intl/intl.dart';

class HomeScreen extends StatefulWidget {

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {

  final numberFormater = new NumberFormat("#,##0.00", "pt_BR");

  EstoqueController   estoqueController   = EstoqueController();
  PagamentoController pagamentoController = PagamentoController();
  EscalaController    escalaController    = EscalaController();
  GeralController     geralController     = GeralController();

  Future<Escala>        _escalaFuture;
  Future<List<Estoque>> _estoqueFuture;
  Future<Pagamento>     _pagamentoFuture;
  Future<Geral>         _geralFuture;

  @override
  void initState() {
    _escalaFuture     = escalaController.getEscalaAtual();
    _estoqueFuture    = estoqueController.getEstoque();
    _pagamentoFuture  = pagamentoController.getPagamentoParticipantes();
    _geralFuture      = geralController.getGeral();
  }

  @override
  Widget build(BuildContext context) {

    Widget _bodyBack() => Container(
      color: AppColors.primaryColor,
    );

    return Material(
      child: Stack(
        fit: StackFit.expand,
        children: <Widget>[

          _bodyBack(),

          RefreshIndicator(
            onRefresh: atualizar,
            child: CustomScrollView(

              slivers: <Widget>[

                _appBar(),

                _escalaList(),

                _separator(),

                _estoqueList(),

                _separator(),

                _pagamentoList(),

                _separator(),

                _saldoEmCaixa(),

                _separator(),
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget _escalaList() {

    return SliverToBoxAdapter(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[

            _getTitleContainter("Escala"),

            FutureBuilder<Escala>(
              future: _escalaFuture,
              builder: (context, snapshot){

                if (!snapshot.hasData) {
                  return _getProgress(AppColors.tercearyColor);
                } else {

                  return Container(
                    height: 150,
                    color: AppColors.tercearyColor,
                    child: ListView.builder(
                        padding: EdgeInsets.only(left: 5.0),
                        scrollDirection: Axis.horizontal,
                        itemCount: snapshot.data.cafezeiros.length,
                        itemBuilder: (context, index) {
                          return _escalaCard(snapshot.data.cafezeiros[index]);
                        }
                    ),
                  );
                }
              },
            ),
          ],
        )
    );
  }

  Widget _estoqueList() {

    return SliverToBoxAdapter(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[

            _getTitleContainter("Estoque"),

            FutureBuilder<List<Estoque>>(
              future: _estoqueFuture,
              builder: (context, snapshot){
                if (!snapshot.hasData) {
                  return _getProgress(AppColors.tercearyColor);
                } else {

                  return Container(
                    height: 150,
                    color: AppColors.tercearyColor,
                    child: ListView.builder(
                        padding: EdgeInsets.only(left: 5.0),
                        scrollDirection: Axis.horizontal,
                        itemCount: snapshot.data.length,
                        itemBuilder: (context, index) {
                          return _estoqueCard(snapshot.data[index]);
                        }
                    ),
                  );
                }
              },
            ),
          ],
        )
    );
  }

  Widget _pagamentoList() {

    return SliverToBoxAdapter(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[

            _getTitleContainter("Pagamento"),

            FutureBuilder<Pagamento>(
              future: _pagamentoFuture,
              builder: (context, snapshot){
                if (!snapshot.hasData) {
                  return _getProgress(AppColors.tercearyColor);
                } else {

                  return Container(
                    height: 150,
                    color: AppColors.tercearyColor,
                    child: ListView.builder(
                        padding: EdgeInsets.only(left: 5.0),
                        scrollDirection: Axis.horizontal,
                        itemCount: snapshot.data.participantes.length,
                        itemBuilder: (context, index) {
                          return _pagamentoCard(snapshot.data.participantes[index]);
                        }
                    ),
                  );
                }
              },
            ),

          ],
        )
    );
  }

  Widget _escalaCard(Cafezeiro cafezeiro){

    bool passou = cafezeiro.data.isBefore(DateTime.now());
    String data = DateHelper.convertToDateString(cafezeiro.data.millisecondsSinceEpoch);
    bool hoje = data == DateHelper.convertToDateString(DateTime.now().millisecondsSinceEpoch);
    EDiaSemana diaSemanaEnum = DiaSemanaHelper.getDiaSemana(cafezeiro.data.weekday);
    String diaSemana = DiaSemanaHelper.getAbreviacao(diaSemanaEnum);
    Color corTexto = hoje ? AppColors.primaryColor : AppColors.secondaryColor;
    corTexto = passou ? Colors.grey : corTexto;
    Color corCard = hoje ? AppColors.secondaryColor : AppColors.primaryColor;

    return Container(
      width: 150.0,
      color: AppColors.tercearyColor,
      child: GestureDetector(
        child: Card(
          margin: EdgeInsets.symmetric(horizontal: 8.0, vertical: 22.0),
          elevation: 10.0,
          color: corCard,
          child: Column(
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Padding(
                  padding: EdgeInsets.only(top: 15.0),
                  child: Column(
                    children: <Widget>[
                      Text(data, style : TextStyle(fontSize: 17, color: corTexto, fontWeight: FontWeight.bold)),
                      Text(diaSemana, style : TextStyle(fontSize: 40, color: corTexto, fontWeight: FontWeight.bold))
                    ],
                  )
              ),
              Text("${cafezeiro.pessoa.nome}", style: TextStyle(color: Colors.white, fontSize: 22),)
            ],
          ),
        ),
      ),
    );

  }

  Widget _estoqueCard(Estoque estoque){

    return Container(
      width: 165.0,
      color: AppColors.tercearyColor,
      child: GestureDetector(
        onTap: (){
        },
        child: Card(
          margin: EdgeInsets.symmetric(horizontal: 8.0, vertical: 22.0),
          elevation: 10.0,
          color: AppColors.primaryColor,
          child: Column(
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(top: 15.0),
                child: Image(image: new AssetImage("assets/images/${estoque.produto.icone}.png"), width: 50,),
              ),
              Padding(
                  padding: EdgeInsets.only(top: 10.0),
                  child: Text("${estoque.quantidade} ${estoque.produto.unidadeMedida} ${estoque.produto.nome}", style: TextStyle(color: Colors.white, fontSize: 18),)
              ),
            ],
          ),
        ),
      ),
    );

  }

  Widget _pagamentoCard(ParticipantePagamento participantePagamento){

    return Container(
      width: 165.0,
      color: AppColors.tercearyColor,
      child: GestureDetector(
        child: Card(
          margin: EdgeInsets.symmetric(horizontal: 8.0, vertical: 22.0),
          elevation: 10.0,
          color: AppColors.primaryColor,
          child: Column(
            children: <Widget>[
              Padding(
                  padding: EdgeInsets.only(top: 30.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text("R\$${numberFormater.format(participantePagamento.valor)}",
                        style: TextStyle(color: AppColors.secondaryColor,
                            fontSize: 25,
                            fontWeight:
                            FontWeight.bold),)
                    ],
                  )
              ),
              Padding(
                  padding: EdgeInsets.only(top: 10.0),
                  child: Text("${participantePagamento.pessoa.nome}", style: TextStyle(color: Colors.white, fontSize: 18),)
              ),
            ],
          ),
        ),
      ),
    );

  }

  Widget _saldoEmCaixa() {

    return SliverToBoxAdapter(
        child: Container(
            margin: EdgeInsets.symmetric(vertical: 30, horizontal: 5),
            color: AppColors.primaryColor,
            child: Column(
              children: <Widget>[

                FutureBuilder<Geral>(
                  future: _geralFuture,
                  builder: (context, snapshot){
                    if (!snapshot.hasData) {
                      return _getProgress(AppColors.primaryColor);
                    } else {

                      return Text("R\$${numberFormater.format(snapshot.data.saldo)}",
                              style: TextStyle(
                              color: AppColors.secondaryColor,
                              fontSize: 50,
                              fontWeight: FontWeight.bold),
                      );
                    }
                  },
                ),

                Text("em caixa",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 25,
                      fontWeight: FontWeight.bold),
                )
              ],
            )
        )
    );
  }

  Widget _getTitleContainter(String text){
    return Container(
        color: AppColors.tercearyColor,
        child: Padding(
          padding: EdgeInsets.only(left: 12.0, top: 15.0),
          child: Text("$text", style: TextStyle(color: AppColors.primaryColor, fontWeight: FontWeight.bold, fontSize: 20),),
        )
    );
  }

  Widget _separator() {

    return SliverToBoxAdapter(
      child: Container(height: 30,),
    );
  }

  Widget _appBar() {

    return SliverAppBar(
      floating: true,
      snap: true,
      backgroundColor: Colors.transparent,
      elevation: 0.0,
      flexibleSpace: FlexibleSpaceBar(
        title: const Text("HOME", style: TextStyle(fontSize: 30),),
        centerTitle: true,
      ),
    );
  }

  Widget _getProgress(Color backColor){
    return Container(
      color: backColor,
      height: 150,
      alignment: Alignment.center,
      child: CircularProgressIndicator(
        valueColor: AlwaysStoppedAnimation<Color>(AppColors.secondaryColor),
      ),
    );
  }

  Future<Null> atualizar() async  {

    setState(() {
      _escalaFuture    = Future.value();
      _estoqueFuture   = Future.value();
      _pagamentoFuture = Future.value();
      _geralFuture     = Future.value();
    });

    Completer<Null> completer = Completer<Null>();

    atualizaDadosFuture().then((atualizado){
      completer.complete();
    });

    return null;
  }

  Future<bool> atualizaDadosFuture() async {

    Completer<bool> completer = Completer<bool>();

    atualizarDados().then((atualizado){
      completer.complete(atualizado);
    });


    return completer.future;

  }

  Future<bool> atualizarDados() async {

    var escala = Future.value(await escalaController.getEscalaAtual());
    setState(() {
      _escalaFuture = escala;
    });

    var estoque = Future.value(await estoqueController.getEstoque());
    setState(() {
      _estoqueFuture = estoque;
    });

    var pagamento = Future.value(await pagamentoController.getPagamentoParticipantes());
    setState(() {
      _pagamentoFuture = pagamento;
    });

    var geral = Future.value(await geralController.getGeral());
    setState(() {
      _geralFuture = geral;
    });

    return true;
  }

}

