import 'package:escala_cafe_app/home/home_pageview.dart';
import 'package:escala_cafe_app/util/AppColors.dart';
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Escala Café',
      theme: ThemeData(
        accentColor: AppColors.secondaryColor,
        primaryColor: AppColors.primaryColor,
      ),
      home: HomePageView(),
    );
  }
}