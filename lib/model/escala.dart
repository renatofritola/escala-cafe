import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:escala_cafe_app/model/cafezeiro.dart';

class Escala{

  String id;
  DateTime inicio;
  DateTime fim;
  List<Cafezeiro> cafezeiros;

  Escala.fromDocument(DocumentSnapshot snapshot){
    this.id     = snapshot.documentID;
    Timestamp timestampInicio = snapshot.data["inicio"];
    Timestamp timestampFim    = snapshot.data["fim"];
    this.inicio = DateTime.fromMillisecondsSinceEpoch(timestampInicio.millisecondsSinceEpoch);
    this.fim    = DateTime.fromMillisecondsSinceEpoch(timestampFim   .millisecondsSinceEpoch);
    this.cafezeiros = List();
  }

}