import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:escala_cafe_app/model/participante_pagamento.dart';

class Pagamento {

  String id;
  bool ativo;
  DateTime inicio;
  DateTime fim;
  List<ParticipantePagamento> participantes;

  Pagamento.fromDocument(DocumentSnapshot snapshot){
    this.id = snapshot.documentID;
    this.ativo = snapshot.data["ativo"];
    var teste = snapshot.data["inicio"];
    print(teste);
  }

}