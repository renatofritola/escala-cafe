import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:escala_cafe_app/model/pessoa.dart';

class ParticipantePagamento{

  String id;
  Pessoa pessoa;
  DocumentReference pessoaReference;
  bool pagou;
  double valor;

  ParticipantePagamento.fromDocument(DocumentSnapshot snapshot){
    this.id              = snapshot.documentID;
    this.pagou           = snapshot.data["pagou"];
    this.pessoaReference = snapshot.data["pessoa"];
    try {
      this.valor = snapshot.data["valor"];
    }catch(Exception){
      int value = snapshot.data["valor"];
      this.valor = value.toDouble();
    }
  }

}