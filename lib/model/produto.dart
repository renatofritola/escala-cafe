import 'package:cloud_firestore/cloud_firestore.dart';

class Produto{

  String id;
  String icone;
  String nome;
  String unidadeMedida;

  Produto.fromDocument(DocumentSnapshot snapshot){
    id    = snapshot.documentID;
    icone = snapshot.data["icone"];
    nome  = snapshot.data["nome"];
    unidadeMedida = snapshot.data["unidade-medida"];
  }
}