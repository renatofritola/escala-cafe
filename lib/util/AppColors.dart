import 'package:flutter/painting.dart';

class AppColors {

  static Color primaryColor         = Color.fromARGB(255,37,0,72);
  static Color primaryColorLight    = Color.fromARGB(255,37,0,72);
  static Color tercearyColor        = Color.fromARGB(255, 0, 193, 235);
  static Color secondaryColor       = Color.fromARGB(255, 249, 56, 106);

}